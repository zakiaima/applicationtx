package GUI;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static GUI.Launch.*;

public class App implements Initializable {

    @FXML
    private Pane pane;
    @FXML
    private AnchorPane parent;

    private double xOffset = 0;
    private double yOffset = 0;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        makeStageDraggable();
    }

    private void makeStageDraggable() {
        parent.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        parent.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
                stage.setOpacity(0.7f);
            }
        });
        parent.setOnDragDone((e) -> stage.setOpacity(1.0f));
        parent.setOnMouseReleased((e) -> {
            stage.setOpacity(1.0f);
        });

    }

    @FXML
    void oppenSettings(ActionEvent event) throws IOException {
        System.out.println("clicked");
        Parent fxml = FXMLLoader.load(getClass().getResource("/GUI/Settings.fxml"));
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);
    }

    @FXML
    void openUserProfile(ActionEvent event) throws IOException {
        System.out.println("clicked");
        Parent fxml = FXMLLoader.load(getClass().getResource("/GUI/Profile.fxml"));
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);
    }

    @FXML
    void openFiles(ActionEvent event) throws IOException {
        System.out.println("clicked");
        Parent fxml = FXMLLoader.load(getClass().getResource("/GUI/Files.fxml"));
        pane.getChildren().removeAll();
        pane.getChildren().setAll(fxml);
    }


}
