package GUI;

import App.myFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;

public class Files {
    @FXML
    private static TableView<myFile> tableFiles;

    @FXML
    private TableColumn<myFile, Long> size;

    @FXML
    private TableColumn<myFile, String> name;

    @FXML
    private TableColumn<myFile, Boolean> Sync;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    void initialize() {

        size.setCellValueFactory(new PropertyValueFactory<>("size"));

        name.setCellValueFactory(new PropertyValueFactory<>("name"));

        Sync.setCellValueFactory( new PropertyValueFactory<>( "sync"));
        Sync.setCellFactory( tc -> new CheckBoxTableCell<>());


    }

    @FXML
    void updateList(ActionEvent event) {
        ObservableList<myFile> files = FXCollections.observableArrayList(Profile.getFiles());

        tableFiles.setItems(files);
        tableFiles.setEditable( true );

    }

    @FXML
    void Synchro(ActionEvent event) {
        final HashSet<myFile> del = new HashSet<myFile>();
        for( final myFile f : tableFiles.getItems()) {
            if( f.getSync()) {
                del.add( f );
            }
        }
        tableFiles.getItems().removeAll( del );
    }

}
