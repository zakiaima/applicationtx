package GUI;


import com.jfoenix.controls.JFXComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import App.CloudStorage;

import App.myFile;

import App.DropBox;
import App.GoogleDrive;

import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import java.util.ResourceBundle;

public class Profile {
    private static ArrayList<CloudStorage> clouds;
    private int id;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;


    @FXML
    private JFXComboBox<?> listStorageOption;

    @FXML
    void createNewStorage(ActionEvent event) {
        if(listStorageOption.getValue() == "Drop Box"){
            DropBox db = new DropBox(id++);
            clouds.add(db);
        }
        else if(listStorageOption.getValue() == "Google Drive"){
            GoogleDrive g = null;
            try {
                g = new GoogleDrive(id++);
            } catch (IOException | GeneralSecurityException e){
                e.printStackTrace();
            }
            clouds.add(g);
        }
    }


    @FXML
    void initialize() {
        List<String> list = new ArrayList<>();
        list.add("Drop Box");
        list.add("Google Drive");
        ObservableList obList = FXCollections.observableList(list);
        listStorageOption.getItems().clear();
        listStorageOption.setItems(obList);

        clouds = new ArrayList<CloudStorage>();
        id = 0;
    }



    public static List<myFile> getFiles() {
        if (clouds==null) {return null;}
        List<myFile> myFiles = new ArrayList<myFile>();
        for (CloudStorage c : clouds) {
            myFiles.addAll(c.listFiles());
        }
        return myFiles;
    }


}
