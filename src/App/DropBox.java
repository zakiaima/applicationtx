package App;

import com.dropbox.core.*;
import com.dropbox.core.json.JsonReader;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.users.DbxUserUsersRequests;
import com.dropbox.core.v2.users.FullAccount;
import javafx.scene.control.TextInputDialog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DropBox implements CloudStorage {
    private String authFileOutput;
    private DbxClientV2 client;

    public DropBox(String ACCESS_TOKEN){
        DbxRequestConfig config = new DbxRequestConfig("examples-authorize");
        this.client = new DbxClientV2(config, ACCESS_TOKEN);
    }
    public DropBox(int i){
        try {
            connect("./cred" + i);
            authFileOutput = "./cred" + i;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connect(String argAuthFileOutput)  throws IOException {
        String argAppInfoFile = "./jso";

        // Read app info file (contains app key and app secret)
        DbxAppInfo appInfo;
        try {
            appInfo = DbxAppInfo.Reader.readFromFile(argAppInfoFile);
        } catch (JsonReader.FileLoadException ex) {
            System.err.println("Error reading <app-info-file>: " + ex.getMessage());
            System.exit(1); return;
        }

        // Run through Dropbox API authorization process
        DbxRequestConfig requestConfig = new DbxRequestConfig("examples-authorize");
        DbxWebAuth webAuth = new DbxWebAuth(requestConfig, appInfo);
        DbxWebAuth.Request webAuthRequest = DbxWebAuth.newRequestBuilder()
                .withNoRedirect()
                .build();
        String authorizeUrl = webAuth.authorize(webAuthRequest);

        String code = finalizeConnect(authorizeUrl);
        DbxAuthFinish authFinish;
        try {
            authFinish = webAuth.finishFromCode(code);
            this.client = new DbxClientV2(requestConfig, authFinish.getAccessToken()) ;
        } catch (DbxException ex) {
            System.err.println("Error in DbxWebAuth.authorize: " + ex.getMessage());
            System.exit(1); return;
        }
/*
        System.out.println("Authorization complete.");
        System.out.println("- User ID: " + authFinish.getUserId());
        System.out.println("- Account ID: " + authFinish.getAccountId());
        System.out.println("- Access Token: " + authFinish.getAccessToken());
*/
        // Save auth information to output file.

        DbxAuthInfo authInfo = new DbxAuthInfo(authFinish.getAccessToken(), appInfo.getHost());
        File output = new File(argAuthFileOutput);
        try {
            DbxAuthInfo.Writer.writeToFile(authInfo, output);
            System.out.println("Saved authorization information to \"" + output.getCanonicalPath() + "\".");
        } catch (IOException ex) {
            System.err.println("Error saving to <auth-file-out>: " + ex.getMessage());
            System.err.println("Dumping to stderr instead:");
            DbxAuthInfo.Writer.writeToStream(authInfo, System.err);
            System.exit(1);
        }
    }

    private String finalizeConnect(String authorizeUrl){

        Browser.openLink(authorizeUrl);

        TextInputDialog dialog = new TextInputDialog("%01xFD12");
        dialog.setTitle("Confirm connection");
        dialog.setHeaderText("Connect to your DropBox account");
        dialog.setContentText("Enter the code:");

        //  get the response value.
        String code="";
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            code = result.get().trim();
        }
        return code;
    }

    @Override
    public void disconnect() {
        //do we delete all files from this drive?
        try {
            File auth = new File(authFileOutput);
            auth.delete();
            finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void download(String cloudStoragePath) {

    }

    @Override
    public void upload(File localFile, String cloudStoragePath) {

    }

    @Override
    public List<myFile> listFiles() {
        try{
            ListFolderResult result = client.files().listFolder("");
            System.out.println(result.getEntries().size());

            ArrayList<myFile> listFiles = new ArrayList<>();
            while (true) {
                for (Metadata metadata : result.getEntries()) {
                    listFiles.add(new myFile("DB",metadata.getName(),metadata.getParentSharedFolderId(),"./",1));// todo ParseMetadata for size ...
                }

                if (!result.getHasMore()) {
                    break;
                }

                result = client.files().listFolderContinue(result.getCursor());
            }
            return listFiles;
        }
        catch (DbxException eUp){
            eUp.printStackTrace();
        }
        return null;
    }
    public String getAccountName(){
        try {
            DbxUserUsersRequests r1 = client.users();
            FullAccount account = r1.getCurrentAccount();

            return (account.getName().getDisplayName());
        }
        catch (DbxException ex1) {
            ex1.printStackTrace();
        }
        return null;
    }
}
