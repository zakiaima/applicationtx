package App;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface CloudStorage {
    void connect(String pathToLog) throws IOException;
    void disconnect();
    void download(String cloudStoragePath);
    void upload(File localFile, String cloudStoragePath);
    List<myFile> listFiles();
}
