package App;

public class myFile {
    private String CloudStorage;
    private String name;
    private String id;
    private String format;
    private String pathLocal;

    private long size;
    private long created;
    private long lastModified;

    private Boolean sync;

    public myFile(String drive, String name, String id, String pathLocal, long size){
        this.CloudStorage = drive;
        this.name = name;
        this.created = System.currentTimeMillis();
        this.pathLocal = pathLocal;
        this.size = size;
        this.sync = false;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getCloudStorage() {
        return CloudStorage;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getFormat() {
        return format;
    }

    public String getPathLocal() {
        return pathLocal;
    }

    public long getSize() {
        return size;
    }

    public long getCreated() {
        return created;
    }

    public long getLastModified() {
        return lastModified;
    }

    public Boolean getSync() {
        return sync;
    }
}
